<?php

namespace Router;

trait PatternMatcherTrait{

    /* @var string[] */
    protected array $patternMatchers = [];

    /**
     * {@inheritDoc}
     */
    public function addPatternMatcher(string $alias, string $regex): self{
        $this->patternMatchers[$alias] = $regex;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function addPatternMatchers(array $patternMatches){
        foreach($patternMatches as $alias => $regex){
            if(!is_numeric($alias) && is_string($regex)){
                $this->addPatternMatcher($alias, $regex);
            }
        }
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getPatternMatchers(): array{
        return $this->patternMatchers;
    }

}