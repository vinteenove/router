<?php

namespace Router;

use Router\Middleware\{MiddlewareAwareInterface, MiddlewareAwareTrait};
use Router\Strategy\{StrategyAwareInterface, StrategyAwareTrait};

class RouteGroup implements
    MiddlewareAwareInterface,
    RouteCollectionInterface,
    StrategyAwareInterface,
    PatternMatcherInterface,
	RouteConditionHandlerInterface
{
    use MiddlewareAwareTrait;
    use RouteCollectionTrait;
    use StrategyAwareTrait;
    use PatternMatcherTrait;
    use RouteConditionHandlerTrait;

    protected string $prefix;

    /* @var string|callable $callback  */
    protected $callback;

    protected RouteCollectionInterface $collection;

    public function __construct(string $prefix, callable $callback, RouteCollectionInterface $collection){
        $this->prefix = sprintf('/%s', ltrim($prefix, '/'));
        $this->callback = $callback;
        $this->collection = $collection;
    }

    /**
     * @inheritDoc
     */
    public function map(?string $method, string $path, $handler): Route{
        $path = ($path === '/') ? $this->prefix : $this->prefix . sprintf('/%s', ltrim($path, '/'));
        $route = $this->collection->map($method, $path, $handler);
        $route->setGroup($this);
        return $route;
    }

    public function __invoke(){
        ($this->callback)($this);
    }

}