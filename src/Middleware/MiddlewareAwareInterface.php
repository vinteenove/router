<?php

namespace Router\Middleware;

interface MiddlewareAwareInterface extends
    MiddlewareAwareSettingInterface,
    MiddlewareAwareGettingInterface
{}