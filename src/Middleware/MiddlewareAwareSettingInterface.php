<?php

namespace Router\Middleware;

use \Psr\Http\Server\MiddlewareInterface;

interface MiddlewareAwareSettingInterface{

    /**
     * Add a middleware to the stack
     *
     * @param MiddlewareInterface $middleware
     *
     * @return MiddlewareAwareInterface
     */
    public function middleware(MiddlewareInterface $middleware): MiddlewareAwareInterface;

    /**
     * Add multiple middleware to the stack
     *
     * @param MiddlewareInterface[] $middlewares
     *
     * @return static
     */
    public function middlewares(array $middlewares): MiddlewareAwareInterface;

    /**
     * Prepend a middleware to the stack
     *
     * @param MiddlewareInterface $middleware
     *
     * @return static
     */
    public function prependMiddleware(MiddlewareInterface $middleware): MiddlewareAwareInterface;

    /**
     * Prepend middlewares to the stack
     *
     * @param array $middlewares
     *
     * @return static
     */
    public function prependMiddlewares(array $middlewares): MiddlewareAwareInterface;

}