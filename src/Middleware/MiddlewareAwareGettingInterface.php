<?php

namespace Router\Middleware;

use \Psr\Http\Server\MiddlewareInterface;

interface MiddlewareAwareGettingInterface{

    /**
     * Shift a middleware from beginning of stack
     *
     * @return MiddlewareInterface|null
     */
    public function shiftMiddleware(): MiddlewareInterface;

    /**
     * Get the stack of middleware
     *
     * @return MiddlewareInterface[]
     */
    public function getMiddlewareStack(): iterable;

}