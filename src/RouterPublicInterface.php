<?php

namespace Router;

use Router\Middleware\MiddlewareAwareSettingInterface;
use Router\Middleware\MiddleweraDispatcher;
use Router\Strategy\StrategyAwareSetterInterface;

interface RouterPublicInterface extends
    MiddlewareAwareSettingInterface,
    StrategyAwareSetterInterface,
    MiddleweraDispatcher
{}