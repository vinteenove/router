<?php

namespace Router;

interface RouteNominavelInterface{

	/**
	 * @param string $name
	 * @return $this
	 */
	public function setName(string $name): self;

	/**
	 * @return string|null
	 */
	public function getName(): ?string;

}