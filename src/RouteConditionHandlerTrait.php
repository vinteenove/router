<?php

namespace Router;

use RuntimeException;

/**
 * Trait RouteConditionHandlerTrait
 * @package Router
 */
trait RouteConditionHandlerTrait {

	/**
	 * @var ?string
	 */
	protected ?string $host = null;

	/**
	 * @var ?int
	 */
	protected ?int $port = null;

	/**
	 * @var ?string
	 */
	protected ?string $scheme = null;

	/**
	 * @var ?string
	 */
	protected ?string $path = null;

	/**
	 * @return string|null
	 */
	public function getHost(): ?string{
		return $this->host;
	}

	/**
	 * @return int|null
	 */
	public function getPort(): ?int{
		return $this->port;
	}

	/**
	 * @return string|null
	 */
	public function getScheme(): ?string{
		return $this->scheme;
	}

	/**
	 * @return string|null
	 */
	public function getPath(): ?string{
		return $this->path;
	}

	/**
	 * @param string $host
	 * @return $this
	 */
	public function setHost(string $host): self{
		$this->host = $host;
		return $this->checkAndReturnSelf();
	}

	/**
	 * @param int|null $port
	 * @return $this
	 */
	public function setPort(?int $port): self{
		$this->port = $port;
		return $this->checkAndReturnSelf();
	}

	/**
	 * @param string $scheme
	 * @return $this
	 */
	public function setScheme(string $scheme): self{
		$this->scheme = $scheme;
		return $this->checkAndReturnSelf();
	}

	/**
	 * @param string $path
	 * @return $this
	 */
	public function setPath(string $path): self{
		$this->path = $path;
		return $this->checkAndReturnSelf();
	}

	/**
	 * @return $this
	 */
	private function checkAndReturnSelf(): self{
		if($this instanceof RouteConditionHandlerInterface){
			return $this;
		}

		throw new RuntimeException(sprintf(
			'Trait (%s) must be consumed by an instance of (%s)',
			__TRAIT__,
			RouteConditionHandlerInterface::class
		));
	}

}