<?php


namespace Router;


interface PatternMatcherInterface{

    /**
     * Add Pattern.
     *
     * @param string $alias
     * @param string $regex
     * @return void
     */
    public function addPatternMatcher(string $alias, string $regex);

    /**
     * Add Multiple Patterns.
     *
     * @param string[] $patternMatches
     * @return void
     */
    public function addPatternMatchers(array $patternMatches);

    /**
     * Returns the list of patterns.
     *
     * @return string[]
     */
    public function getPatternMatchers(): array;

}