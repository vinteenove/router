<?php

namespace Router;

trait RouteNominavelTrait {

	/**
	 * @var ?string
	 */
	protected ?string $name = null;

	/**
	 * @return string|null
	 */
	public function getName(): ?string{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return $this
	 */
	public function setName(string $name): self{
		$this->name = $name;
		return $this;
	}

}