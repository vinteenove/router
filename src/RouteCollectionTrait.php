<?php

namespace Router;

/**
 * Trait RouteCollectionTrait
 *
 * @package Router
 */
trait RouteCollectionTrait {

	/**
	 * Adiciona uma rota no mapa
	 *
	 * @param string   $method GET|POST|PUT|PATCH|DELETE|HEAD|OPTIONS|[* para ANY]
	 * @param string   $path
	 * @param callable $handler
	 * @return mixed
	 */
	abstract public function map(string $method, string $path, $handler): Route;

	/**
	 * @param string   $path
	 * @param callable $handler
	 * @return Route
	 */
	public function get(string $path, $handler): Route{
		return $this->map('GET', $path, $handler);
	}

	/**
	 * @param string   $path
	 * @param callable $handler
	 * @return Route
	 */
	public function post(string $path, $handler): Route{
		return $this->map('POST', $path, $handler);
	}

	/**
	 * @param string   $path
	 * @param callable $handler
	 * @return Route
	 */
	public function put(string $path, $handler): Route{
		return $this->map('PUT', $path, $handler);
	}

	/**
	 * @param string   $path
	 * @param callable $handler
	 * @return Route
	 */
	public function patch(string $path, $handler): Route{
		return $this->map('PATCH', $path, $handler);
	}

	/**
	 * @param string   $path
	 * @param callable $handler
	 * @return Route
	 */
	public function delete(string $path, $handler): Route{
		return $this->map('DELETE', $path, $handler);
	}

	/**
	 * @param string   $path
	 * @param callable $handler
	 * @return Route
	 */
	public function head(string $path, $handler): Route{
		return $this->map('HEAD', $path, $handler);
	}

	/**
	 * @param string   $path
	 * @param callable $handler
	 * @return Route
	 */
	public function options(string $path, $handler): Route{
		return $this->map('OPTIONS', $path, $handler);
	}

	/**
	 * @param string   $path
	 * @param callable $handler
	 * @return Route
	 */
	public function any(string $path, $handler): Route{
		return $this->map('*', $path, $handler);
	}

}