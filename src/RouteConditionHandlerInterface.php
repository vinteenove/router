<?php

namespace Router;

/**
 * Interface RouteConditionHandlerInterface
 * @package Router
 */
interface RouteConditionHandlerInterface{

	/**
	 * @return string|null
	 */
	public function getHost(): ?string;

	/**
	 * @return int|null
	 */
	public function getPort(): ?int;

	/**
	 * @return string|null
	 */
	public function getScheme(): ?string;

	/**
	 * @return string|null
	 */
	public function getPath(): ?string;

	/**
	 * @param string $host
	 * @return $this
	 */
	public function setHost(string $host): self;

	/**
	 * @param int|null $port
	 * @return $this
	 */
	public function setPort(?int $port): self;

	/**
	 * @param string $scheme
	 * @return $this
	 */
	public function setScheme(string $scheme): self;

	/**
	 * @param string $path
	 * @return $this
	 */
	public function setPath(string $path): self;

}