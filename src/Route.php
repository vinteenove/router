<?php

namespace Router;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Router\Middleware\MiddlewareAwareInterface;
use Router\Middleware\MiddlewareAwareTrait;
use RuntimeException;
use Router\Strategy\{
	StrategyAwareInterface,
	StrategyAwareTrait,
	StrategyInterface};

class Route implements
	MiddlewareInterface,
	MiddlewareAwareInterface,
	RouteConditionHandlerInterface,
	StrategyAwareInterface,
	PatternMatcherInterface,
	RouteNominavelInterface
{
	use MiddlewareAwareTrait;
	use StrategyAwareTrait;
	use PatternMatcherTrait;
	use RouteNominavelTrait;
	use RouteConditionHandlerTrait;

	private const PATTERN_DYNAMIC = '/{.+?}|\*$/';

	protected ?string $method;

	protected string $query;

	/** @var callable|string */
	protected $handler;

	protected ?Router $router = null;

	protected ?RouteGroup $group = null;

	protected array $vars = [];

	/**
	 * Construct.
	 *
	 * @param null|string     $method
	 * @param string          $query
	 * @param callable|string $handler
	 */
	public function __construct(?string $method, string $query, $handler)
	{
		$this->method  = $method;
		$this->query   = $query;
		$this->handler = $handler;
	}

	public function getScheme(): ?string{
		$scheme = $this->scheme;

		if($scheme !== null){
			return $scheme;
		}

		if($this->group){
			$scheme = $this->group->getScheme();
		}

		if($scheme !== null){
			return $scheme;
		}

		if($this->router){
			$scheme = $this->router->getScheme();
		}

		if($scheme !== null){
			return $scheme;
		}

		return null;
	}

	public function getHost(): ?string{
		$host = $this->host;

		if($host !== null){
			return $host;
		}

		if($this->group){
			$host = $this->group->getHost();
		}

		if($host !== null){
			return $host;
		}

		if($this->router){
			$host = $this->router->getHost();
		}

		if($host !== null){
			return $host;
		}

		return null;
	}

	public function getPort(): ?int{
		$port = $this->port;

		if($port !== null){
			return $port;
		}

		if($this->group){
			$port = $this->group->getPort();
		}

		if($port !== null){
			return $port;
		}

		if($this->router){
			$port = $this->router->getPort();
		}

		if($port !== null){
			return $port;
		}

		return null;
	}

	public function getPath(): ?string{
		$path = $this->path;

		if($path !== null){
			return $path;
		}

		if($this->group){
			$path = $this->group->getPath();
		}

		if($path !== null){
			return $path;
		}

		if($this->router){
			$path = $this->router->getPath();
		}

		if($path !== null){
			return $path;
		}

		return null;
	}

	/**
	 * @return string|null
	 */
	public function getMethod(): ?string{
		return $this->method;
	}

	/**
	 * @param array $replacements
	 * @return string
	 */
	public function getQuery(array $replacements = []): string
	{
		$toReplace = [];

		foreach ($replacements as $wildcard => $actual) {
			$toReplace['/{' . preg_quote($wildcard, '/') . '(:.*?)?}/'] = $actual;
		}

		return preg_replace(array_keys($toReplace), array_values($toReplace), $this->query);
	}

	public function getUri(array $replacements = []): string{
		return ltrim($this->getQuery($replacements), '/');
	}

	public function getTarget(): string{
		$method = $this->getMethod();
		$scheme = $this->getScheme();
		$port = $this->getPort();
		$host = $this->getHost();
		$path = $this->getPath();
		$query = $this->getQuery();

		$target =
			(($method)? $method . ' ': '').
			(($scheme)? $scheme . '://': '').
			(($host)? $host: '').
			(($port)? ':' . $port: '').
			(($path)? $path: '').
			(($query)? $query: '');

		return $target;
	}

	/**
	 * @param Router $router
	 * @return $this
	 */
	public function setRouter(Router $router): self{
		$this->router = $router;
		return $this;
	}

	/**
	 * @param RouteGroup $group
	 * @return $this
	 */
	public function setGroup(RouteGroup $group): self{
		$this->group = $group;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getVars(): array{
		return $this->vars;
	}

	/**
	 * @param array $vars
	 * @return Route
	 */
	public function setVars(array $vars): self{
		$this->vars = $vars;
		return $this;
	}

	/**
	 * @return callable
	 * @throws InvalidArgumentException
	 */
	public function getCallable(): callable
	{
		$callable = $this->handler;

		if (is_string($callable) && strpos($callable, '::') !== false) {
			$callable = explode('::', $callable);
		}

		if (is_array($callable) && isset($callable[0]) && is_object($callable[0])) {
			$callable = [$callable[0], $callable[1]];
		}

		if (!is_callable($callable)) {
			throw new \Exception('Could not resolve a callable for this route');
		}

		return $callable;
	}

	/** @return bool */
	public function isDynamic(): bool{
		return preg_match(self::PATTERN_DYNAMIC, $this->getQuery());
	}

	/** @return string */
	public function getRegexQuery(): string{
		$patternMatchers = $this->pullPatternMatchers();

		$patterns = [];
		$matchers = [];
		foreach($patternMatchers as $alias => $regex){
			$patterns[] = '/^{([^:]+):' . preg_quote($alias) . '}$/';
			$matchers[] = '(?\'$1\'' . $regex . ')';
		}

		// Generic variable
		$patterns[] = '/^{(.*)}$/';
		$matchers[] = '(?\'$1\'[^\\/]+)';

		$parts = explode('/', $this->query);
		$end = array_pop($parts);

		foreach($parts as $k => $part){
			if(empty($parts)){
				continue;
			}
			$parts[$k] = preg_replace($patterns, $matchers, $part);
		}

		if($end !== null){
			if(empty($end)){
				$parts[$k] = $end;
			}else{
				// Friendly query
				$patterns[] = '/^\*$/';
				$matchers[] = '(?\'query\'.*)';
				$parts[] = preg_replace($patterns, $matchers, $end);
			}
		}

		return '^' . implode('\\/', $parts) . '$';
	}

	public function pullMiddlewares(): iterable{
		$middlewares = $this->getMiddlewareStack();

		if($this->group){
			$middlewares = array_merge($this->group->getMiddlewareStack(), $middlewares);
		}

		if($this->router){
			$middlewares = array_merge($this->router->getMiddlewareStack(), $middlewares);
		}

		return $middlewares;
	}

	public function pullPatternMatchers(): array{
		$patternMatchers = $this->getPatternMatchers();

		if($this->group){
			$patternMatchers = array_merge($this->group->getPatternMatchers(), $patternMatchers);
		}

		if($this->router){
			$patternMatchers = array_merge($this->router->getPatternMatchers(), $patternMatchers);
		}

		return $patternMatchers;
	}

	public function pullStrategy(?StrategyInterface $strategyStandard = null): ?StrategyInterface{

		if($strategy = $this->getStrategy()){
			return $strategy;
		}

		if($this->group && $strategy = $this->group->getStrategy()){
			$this->setStrategy($strategy);
			return $strategy;
		}

		if($this->router && $strategy = $this->router->getStrategy()){
			$this->setStrategy($strategy);
			return $strategy;
		}

		if($strategyStandard){
			$this->setStrategy($strategyStandard);
			return $strategyStandard;
		}

		return null;
	}

	/** @inheritDoc */
	public function process(
		ServerRequestInterface $request,
		RequestHandlerInterface $handler
	): ResponseInterface{
		if($strategy = $this->pullStrategy()){
			return $strategy->invokeRouteCallable($this, $request);
		}
		throw new RuntimeException('Strategy was not found');
	}

	public function __debugInfo(){
		return[
			'target'  => $this->getTarget(),
			'handler' => $this->handler,
		];
	}

}