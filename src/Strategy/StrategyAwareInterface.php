<?php

namespace Router\Strategy;

interface StrategyAwareInterface extends
    StrategyAwareGetterInterface,
    StrategyAwareSetterInterface
{}