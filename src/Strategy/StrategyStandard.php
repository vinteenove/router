<?php

namespace Router\Strategy;

use Lliure\Http\Message\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Router\Route;
use Throwable;

class StrategyStandard extends AbstractStrategy implements
    StrategyInterface
{
    use StrategyAwareTrait;

    /** @inheritDoc */
    public function invokeRouteCallable(Route $route, ServerRequestInterface $request): ResponseInterface{

        //$controller = $route->getCallable($this->getContainer());
        $controller = $route->getCallable();

        $controllerResponse = $controller($request, $route->getVars());

        if($controllerResponse instanceof ResponseInterface){
            return $controllerResponse;
        }

        $response = new Response();

        if(is_string($controllerResponse)){
            $response->getBody()->write($controllerResponse);
        }

        return $response;
    }

    /** @inheritDoc */
    public function getUnsolvedDecorator(Throwable $exception): MiddlewareInterface
    {
        return new class($exception) implements MiddlewareInterface
        {
            protected Throwable $error;

            public function __construct(Throwable $error)
            {
                $this->error = $error;
            }

            public function process(
                ServerRequestInterface $request,
                RequestHandlerInterface $handler
            ): ResponseInterface {
                throw $this->error;
            }
        };
    }

    /** @inheritDoc */
    public function getThrowableHandler(): MiddlewareInterface
    {
        return new class implements MiddlewareInterface
        {
            /**
             * @inheritDoc
             *
             * @throws Throwable
             */
            public function process(
                ServerRequestInterface $request,
                RequestHandlerInterface $handler
            ): ResponseInterface {
                try {
                    return $handler->handle($request);
                } catch (Throwable $e) {
                    throw $e;
                }
            }
        };
    }

}