<?php declare(strict_types=1);

namespace Router\Strategy;

use Psr\Http\Message\{ResponseInterface, ServerRequestInterface};
use Psr\Http\Server\MiddlewareInterface;
use Router\Route;
use Throwable;

interface StrategyInterface{

    /**
     * Invoke the route callable based on the strategy
     *
     * @param Route                  $route
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function invokeRouteCallable(Route $route, ServerRequestInterface $request): ResponseInterface;

    /**
     * Get a middleware that will decorate a NotFoundException or NotAllowedException
     *
     * @param Throwable $exception
     *
     * @return MiddlewareInterface
     */
    public function getUnsolvedDecorator(/*NotFoundException*/ Throwable $exception): MiddlewareInterface;

    /**
     * Get a middleware that acts as a throwable handler, it should wrap the rest of the
     * middleware stack and catch any throwables.
     *
     * @return MiddlewareInterface
     */
    public function getThrowableHandler(): MiddlewareInterface;
}
