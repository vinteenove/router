<?php

namespace Router\Strategy;

interface StrategyAwareSetterInterface
{
    /**
     * Set the strategy implementation
     *
     * @param StrategyInterface $strategy
     *
     * @return static
     */
    public function setStrategy(StrategyInterface $strategy): StrategyAwareInterface;
}
