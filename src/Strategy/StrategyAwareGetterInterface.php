<?php

namespace Router\Strategy;

interface StrategyAwareGetterInterface
{
    /**
     * Get the current strategy
     *
     * @return StrategyInterface
     */
    public function getStrategy(): ?StrategyInterface;
}
