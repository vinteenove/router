<?php

namespace Router;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Router\Middleware\{MiddlewareAwareTrait, MiddlewareAwareInterface};

class Dispatcher implements
    MiddlewareAwareInterface,
    RequestHandlerInterface
{
    use MiddlewareAwareTrait;

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface{
        $middleware = $this->shiftMiddleware();
        return $middleware->process($request, $this);
    }
}