<?php

namespace Router;

use Helpers\HttpHelper;
use Lliure\Http\Exception\HttpException;
use InvalidArgumentException;
use Router\Middleware\{
	MiddlewareAwareInterface,
	MiddlewareAwareTrait,
	MiddleweraDispatcher};
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Router\Strategy\{
	StrategyAwareInterface,
	StrategyAwareTrait,
	StrategyInterface,
	StrategyStandard};

class Router implements
	RequestHandlerInterface,
	MiddleweraDispatcher,
	RouteCollectionInterface,
	PatternMatcherInterface,
	StrategyAwareInterface,
	RouterPublicInterface,
	MiddlewareAwareInterface,
	RouteConditionHandlerInterface
{
	use MiddlewareAwareTrait;
	use PatternMatcherTrait;
	use StrategyAwareTrait;
	use RouteCollectionTrait;
	use RouteConditionHandlerTrait;

	private const FOUND = 1;
	private const METHOD_NOT_ALLOWED = 2;
	private const NOT_FOUND = 3;

	/** @var Route[] */
	protected array $routes = [];

	/** @var RouteGroup[] */
	protected array $groups = [];

	/** @var Dispatcher */
	protected Dispatcher $dispatcher;

	public function __construct(?Dispatcher $dispatcher = null){
		foreach([
			'number'        => '[0-9]+',
			'word'          => '[a-zA-Z]+',
			'alphanum_dash' => '[a-zA-Z0-9-_]+',
			'slug'          => '[a-z0-9-]+',
			'uuid'          => '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}+',
		] as $alias => $regex){
			$this->addPatternMatcher($alias, $regex);
		}

		$this->dispatcher = $dispatcher ?? new Dispatcher();
	}

	/**
	 * @inheritDoc
	 */
	public function map(?string $method, string $path, $handler): Route{
		$path  = sprintf('/%s', ltrim($path, '/'));
		$route = new Route($method, $path, $handler);
		$route->setRouter($this);
		return $this->routes[] = $route;
	}

	public function group(string $prefix, callable $group): RouteGroup{
		return $this->addGroup(new RouteGroup($prefix, $group, $this));
	}

	public function addGroup(RouteGroup $routeGroup): RouteGroup{
		return $this->groups[] = $routeGroup;
	}

	public function toRouteGroup(string $prefixo, Router $collection): RouteGroup{
		$this->processGroups();

		$group = new RouteGroup($prefixo, function(RouteGroup $router){
			foreach($this->routes as $route){
				$callable = null;

				try{
					$callable = $route->getCallable();
				}catch(\Exception $e){}

				$new = $router->map($route->getMethod(), $route->getQuery(), $callable);

				if($strategy = $route->pullStrategy()){
					$new->setStrategy($strategy);
				}

				$new->middlewares((array) $route->pullMiddlewares());

				$new->addPatternMatchers($route->pullPatternMatchers());

				if($route->getName()){
					$new->setName($route->getName());
				}

				if($route->getScheme()){
					$new->setScheme($route->getScheme());
				}

				if($route->getHost()){
					$new->setHost($route->getHost());
				}

				if($route->getPort()){
					$new->setPort($route->getPort());
				}

				if($route->getPath()){
					$new->setPath($route->getPath());
				}
			}
		}, $collection);

		return $group;
	}

	public function dispatch(ServerRequestInterface $request): ResponseInterface{

		/* @var Route|null $route */
		$requestyUri = $request->getUri();
		$method = $request->getMethod();
		$scheme = $requestyUri->getScheme();
		$host = $requestyUri->getHost();
		$port = $requestyUri->getPort();
		$path = $requestyUri->getPath();
		$query = '/' . ltrim($requestyUri->getQuery(), '/');

		list($status, $route) = $this->dispatchRouter($method, $scheme, $host, $port, $path, $query);

		$strategy = ((!!$route)? $route->pullStrategy(new StrategyStandard): $this->pullStrategy(new StrategyStandard));

		switch($status){
			case self::NOT_FOUND:
				$this->dispatcher->middlewares((array) $this->getMiddlewareStack());
				$this->dispatcher->prependMiddleware($strategy->getThrowableHandler());
				$middleware = $strategy->getUnsolvedDecorator(new HttpException(404, 'Not Found'));
				$this->dispatcher->middleware($middleware);
			break;
			case self::METHOD_NOT_ALLOWED:
				$this->dispatcher->middlewares((array) $this->getMiddlewareStack());
				$this->dispatcher->prependMiddleware($strategy->getThrowableHandler());
				$middleware = $strategy->getUnsolvedDecorator(new HttpException(405, 'Method Not Allowed'));
				$this->dispatcher->middleware($middleware);
			break;
			case self::FOUND:
				$this->dispatcher->middlewares($route->pullMiddlewares());
				$this->dispatcher->prependMiddleware($strategy->getThrowableHandler());
				$this->dispatcher->middleware($route);
			break;
		}

		return $this->handle($request);
	}

	/** @inheritDoc */
	public function handle(ServerRequestInterface $request): ResponseInterface{
		return $this->dispatcher->handle($request);
	}

	/**
	 * @param string $requestMethod
	 * @param string $requestPath
	 * @return array<int, Route>
	 */
	private function dispatchRouter(string $requestMethod, string $requestScheme, string $requestHost, ?int $requestPort, string $requestPath, string $requestQuery): array{
		/* @var Route $route */

		list($routeStaticMap, $routeDynamicMap) = $this->buildStaticAndDynamicMaps();

		if(isset($routeStaticMap[$requestQuery]) && (isset($routeStaticMap[$requestQuery][($method = $requestMethod)]) || isset($routeStaticMap[$requestQuery][($method = '*')]))){
			$route = $routeStaticMap[$requestQuery][$method];
			if(self::testRoute($route, $requestScheme, $requestHost, $requestPort, $requestPath)){
				return [self::FOUND, $route];
			}
		}

		foreach($routeDynamicMap as $currentTargety => $methods){
			if(isset($methods[($method = $requestMethod)]) || isset($methods[($method = '*')])){
				/* @var Route $route */
				$route = $methods[$method];
				if(self::testRoute($route, $requestScheme, $requestHost, $requestPort, $requestPath, $requestQuery)){
					preg_match("/" . $route->getRegexQuery() . "/" , $requestQuery, $matches);
					$route->setVars(self::prepareMatchers($matches));
					return [self::FOUND, $route];
				}
			}
		}

		if(isset($routeStaticMap[$requestQuery]) && !isset($routeStaticMap[$requestQuery][$requestMethod])){
			return [self::METHOD_NOT_ALLOWED, null];
		}

		foreach($routeDynamicMap as $currentTargety => $methods){
			foreach($methods as $method => $route){
				if(self::testRoute($route, $requestScheme, $requestHost, $requestPort, $requestPath, $requestQuery)){
					return [self::METHOD_NOT_ALLOWED, null];
				}
			}
		}

		return [self::NOT_FOUND, null];
	}

	private static function testRoute(Route $route, string $requestScheme, string $requestHost, ?int $requestPort, string $requestPath, ?string $requestQuery = null){
		if($requestQuery !== null){
			$pattern = $route->getRegexQuery();
			if (!preg_match("/$pattern/" , $requestQuery)){
				return false;
			}
		}

		if($route->getScheme() !== null && $route->getScheme() !== $requestScheme){
			return false;
		}

		if($route->getHost() !== null && $route->getHost() !== $requestHost){
			return false;
		}

		if($route->getPort() !== null && $route->getPort() !== $requestPort){
			return false;
		}

		if($route->getPath() !== null && $route->getPath() !== $requestPath){
			return false;
		}

		return true;
	}

	private static function prepareMatchers($matches): array{
		$matches = array_filter($matches, function($k){
			return !is_numeric($k);
		}, ARRAY_FILTER_USE_KEY);

		if(isset($matches['*'])){
			$matches['*'] = HttpHelper::parse_friendly($matches['*']);
		}

		return $matches;
	}

	/**
	 * Get a named route
	 *
	 * @param string $name
	 *
	 * @return Route
	 *
	 * @throws InvalidArgumentException when no route of the provided name exists
	 */
	public function getNamedRoute(string $name): Route
	{
		$this->processGroups();

		$routeNamedMap = $this->buildNameMap();
		if (isset($routeNamedMap[$name])) {
			return $routeNamedMap[$name];
		}
		throw new InvalidArgumentException(sprintf('No route of the name (%s) exists', $name));
	}

	protected function buildStaticAndDynamicMaps(): array{

		$this->processGroups();

		$routesStaticMap = [];
		$routesDynamicsMap = [];
		foreach ($this->routes as $key => $route){
			[$method, $uri] = explode(' ', $route->getTarget(), 2);

			($route->isDynamic()? $routerMap =& $routesDynamicsMap: $routerMap =& $routesStaticMap);

			$routerMap[$uri] = $routerMap[$uri] ?? [];
			$routerMap[$uri][$method] = $route;
		}

		return [$routesStaticMap, $routesDynamicsMap];
	}

	/**
	 * @return Route[]
	 */
	protected function buildNameMap(): array{
		$this->processGroups();

		$routesNamedMap = [];
		foreach ($this->routes as $key => $route) {
			if ($route->getName() !== null) {
				$routesNamedMap[$route->getName()] = $route;
			}
		}
		return $routesNamedMap;
	}

	protected function processGroups(){
		/* @var callable $group  */
		foreach ($this->groups as $key => $group){
			unset($this->groups[$key]);
			$group();
		}
	}

	public function appendRoute(Route $route): self{
		$this->routes[] = $route;
		return $this;
	}

	public function pullStrategy(?StrategyInterface $strategyStandard = null): ?StrategyInterface{

		if($strategy = $this->getStrategy()){
			return $strategy;
		}

		if($strategyStandard){
			$this->setStrategy($strategyStandard);
			return $strategyStandard;
		}

		return null;
	}
}