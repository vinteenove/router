<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Helpers\HttpHelper;
use Lliure\Http\Exception\HttpException;
use Lliure\Http\Message\ServerRequest;
use Lliure\Http\Message\Uri;
use Psr\Http\Message\RequestInterface;
use Router\Router;

$router = new Router();

$router->get('/home', function(RequestInterface $request, $vars): string{
	return 'OK';
});

$method = 'GET';
$uri = 'http://localhost/home';

try{
	$serverRequest = new ServerRequest($method, Uri::fromParts($t = HttpHelper::parseFriendlyUri($uri)));

	$response = $router->dispatch($serverRequest);

	var_dump('OK' == $response->getBody());

}catch(HttpException $httpException){
	echo 'CODE: ' . $httpException->getStatusCode() . ' | ' . $httpException->getMessage();
}
